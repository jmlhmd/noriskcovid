package tn.ipsas.info2.noriskcovid.view;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class JfxView extends HBox {
    private List<NoRiskCovidUserView> users = new ArrayList<>();
    private NoRiskCovidServerView server;
    /** View for the whole application. */
    public JfxView(final Stage stage, final int width,
                   final int height, final int nbUsers) {
        server = new NoRiskCovidServerView();

        // Name of window
        stage.setTitle("NoRiskCovid Simulator");

        final HBox root = this;

        final VBox usersBox = new VBox();
        final ObservableList<NoRiskCovidUserView> usersList = FXCollections.observableArrayList();

        usersBox.getChildren().add(new Label("Users"));

        for (int i = 0; i < nbUsers; i++) {
            final NoRiskCovidUserView u = new NoRiskCovidUserView("User " + i);
            users.add(u);
            usersBox.getChildren().add(u.getGui());
            usersList.add(u);
        }

        root.getChildren().add(usersBox);

        final VBox meetBox = new VBox();
        final Label l = new Label("Proximity simulator");

        final ComboBox<NoRiskCovidUserView> userA = new ComboBox<>();
        final ComboBox<NoRiskCovidUserView> userB = new ComboBox<>();
        userA.setItems(usersList);
        userB.setItems(usersList);
        final Button meetBtn = new Button("Meet!");
        meetBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(final ActionEvent event) {
                final NoRiskCovidUserView a = userA.getValue();
                final NoRiskCovidUserView b = userB.getValue();
                if (a == null || b == null) {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setContentText("Please select two users that will meet");
                    alert.showAndWait();
                    return;
                }
                a.meet(b);
            }
        });


        meetBox.getChildren().addAll(l, new HBox(userA, userB), meetBtn,
            new Separator(), server.getGui());

        root.getChildren().addAll(new Separator(), meetBox);

        final Scene scene = new Scene(root, width, height);
        stage.setScene(scene);
        stage.show();
    }

    NoRiskCovidServerView getServer() {
        return server;
    }

    List<NoRiskCovidUserView> getUsers() {
        return users;
    }
}
